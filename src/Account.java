public class Account {
    private double score;

    public Account(double score) {
        this.score = score;
    }

    public double getScore() {
        return score;
    }

    public synchronized void setScore(double score) {
        this.score = score;
    }

    public synchronized void transfer(Account fromAccount, Account toAccount, double amount) {
        synchronized (fromAccount){
            try {
                Thread.sleep(1000); //deadlock
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (toAccount){
                if(fromAccount.getScore() >= amount) {
                    fromAccount.setScore(fromAccount.getScore() - amount);
                    toAccount.setScore(toAccount.getScore() + amount);
                }
            }
        }
    }
}
