public class Demo {
    public static void main(String[] args) {

        Account fromAccount = new Account(500);
        Account toAccount = new Account(200);

        Thread thread1 = new Thread(() -> fromAccount.transfer(fromAccount, toAccount, 100));

        Thread thread2 = new Thread(() -> toAccount.transfer(toAccount, fromAccount, 50));

        thread1.start();
        thread2.start();

        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(fromAccount.getScore());
        System.out.println(toAccount.getScore());
    }
}
